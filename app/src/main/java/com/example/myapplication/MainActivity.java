package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private TextToSpeech tts;
    private SpeechRecognizer sr;
    private Intent srIntent;
    private final int RecordAudioRequestCode = 1;
    private boolean isListening = false;


    private ArrayList<String> expression;
    private String currentNumber = "";

    private boolean needToClean = false;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        expression = new ArrayList<String>();

        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.getDefault());
                }
            }
        });

        sr = SpeechRecognizer.createSpeechRecognizer(this);

        srIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        srIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        srIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());


        sr.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {
                TextView listeningText = (TextView) findViewById(R.id.textViewListening);
                listeningText.setText(R.string.readyToListenTxt);
            }

            @Override
            public void onBeginningOfSpeech() {
                TextView listeningText = (TextView) findViewById(R.id.textViewListening);
                listeningText.setText(R.string.listeningTxt);
            }

            @Override
            public void onRmsChanged(float v) {}

            @Override
            public void onBufferReceived(byte[] bytes) {}

            @Override
            public void onEndOfSpeech() {}

            @Override
            public void onError(int i) {}

            @Override
            public void onResults(Bundle bundle) {
                TextView listeningText = (TextView) findViewById(R.id.textViewListening);
                listeningText.setText("");

                ArrayList<String> data = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                System.out.println(data.get(0));

                String voiceData = data.get(0);
                String[] parts = voiceData.split(" ");

                ArrayList<String> voiceExpr = new ArrayList<String>();
                Collections.addAll(voiceExpr, parts);
                for (int i = 0; i < voiceExpr.size(); i++) {
                    if (voiceExpr.get(i).equals(getResources().getString(R.string.additionSpeech))) {
                        voiceExpr.set(i, "+");
                    } else if (voiceExpr.get(i).equals(getResources().getString(R.string.subtractionSpeech))) {
                        voiceExpr.set(i, "-");
                    } else if (voiceExpr.get(i).equals(getResources().getString(R.string.multiplicationSpeech))) {
                        voiceExpr.set(i, "*");
                    } else if (voiceExpr.get(i).equals(getResources().getString(R.string.divisionSpeech))) {
                        voiceExpr.set(i, "/");
                    }
                }

                if (needToClean &&
                        (voiceExpr.get(0).equals("+") ||
                    voiceExpr.get(0).equals("-") ||
                    voiceExpr.get(0).equals("*") ||
                    voiceExpr.get(0).equals("/"))
                ) {
                    expression.add(currentNumber);
                    needToClean = false;
                }
                else {
                    expression.clear();
                    currentNumber = "";
                }

                for (int i = 0; i < voiceExpr.size(); i++) {
                    expression.add(voiceExpr.get(i));
                }

                calculateResult();
            }

            @Override
            public void onPartialResults(Bundle bundle) {}

            @Override
            public void onEvent(int i, Bundle bundle) {}
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        if (sr != null) {
            sr.destroy();
        }
    }

    //Digits (and dot) buttons click event
    public void onClickNumber (View view) {
        if (needToClean) {
            currentNumber = "";
            needToClean = false;
        }

        switch ((String)view.getTag()) {
            case "btn0":
                currentNumber += "0";
                break;
            case "btn1":
                currentNumber += "1";
                break;
            case "btn2":
                currentNumber += "2";
                break;
            case "btn3":
                currentNumber += "3";
                break;
            case "btn4":
                currentNumber += "4";
                break;
            case "btn5":
                currentNumber += "5";
                break;
            case "btn6":
                currentNumber += "6";
                break;
            case "btn7":
                currentNumber += "7";
                break;
            case "btn8":
                currentNumber += "8";
                break;
            case "btn9":
                currentNumber += "9";
                break;
            case "btnDot":
                currentNumber += ".";
        }

        updateResultText();
    }

    //Operation button (+,-,*,/) click event
    public void onClickOperation (View view) {
        String op;
        switch ((String)view.getTag()) {
            case "btnAdd":
                op = "+";
                break;
            case "btnSub":
                op = "-";
                break;
            case "btnMul":
                op = "*";
                break;
            case "btnDiv":
                op = "/";
                break;
            default:
                op = "";
                break;
        }

        expression.add(currentNumber);
        expression.add(op);

        currentNumber = "";
        updateResultText();
    }

    //Clear button click event
    public void onClickClear (View view) {
        expression.clear();

        currentNumber = "";
        updateResultText();
    }

    //Equal button click event
    public void onClickEqual (View view) {
        expression.add(currentNumber);
        calculateResult();
    }

    public void onClickSpeak (View view) {
        tts.speak(currentNumber, TextToSpeech.QUEUE_FLUSH, null, null);
    }

    public void onClickListen (View view) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        RecordAudioRequestCode
                );
            }
        }

        if (isListening){
            sr.stopListening();
        }
        else {
            TextView listeningText = (TextView)findViewById(R.id.textViewListening);
            sr.startListening(srIntent);
        }
    }

    private void calculateResult () {
        //Save current expression string for later displaying
        String expressionStr = exprToString();

        double result = processExpression();

        currentNumber = Double.valueOf(result).toString();
        expression.clear();
        updateResultText();

        //Display the original expression
        TextView expressionText = (TextView)findViewById(R.id.textViewExpression);
        expressionText.setText(expressionStr.toString());

        needToClean = true;
    }

    private double processExpression () {
        double result = 0.0;
        try {
            //Look for divisions and multiplications
            int opIndex = 0;
            for (int i = 1; i < expression.size(); i += 2) {
                if (expression.get(i).equals("*")) {
                    double n1 = Double.parseDouble(expression.get(i-1));
                    double n2 = Double.parseDouble(expression.get(i+1));

                    double res = n1 * n2;
                    expression.set(i, Double.valueOf(res).toString());
                    expression.remove(i-1);
                    expression.remove(i);

                    i = -1;
                }
                else if (expression.get(i).equals("/")) {
                    double n1 = Double.parseDouble(expression.get(i-1));
                    double n2 = Double.parseDouble(expression.get(i+1));

                    double res = n1 / n2;
                    expression.set(i, Double.valueOf(res).toString());
                    expression.remove(i-1);
                    expression.remove(i);

                    i = -1;
                }
            }

            //Look for addition and subtraction
            for (int i = 1; i < expression.size(); i += 2) {
                if (expression.get(i).equals("+")) {
                    double n1 = Double.parseDouble(expression.get(i - 1));
                    double n2 = Double.parseDouble(expression.get(i + 1));

                    double res = n1 + n2;
                    expression.set(i, Double.valueOf(res).toString());
                    expression.remove(i - 1);
                    expression.remove(i);

                    i = -1;
                } else if (expression.get(i).equals("-")) {
                    double n1 = Double.parseDouble(expression.get(i - 1));
                    double n2 = Double.parseDouble(expression.get(i + 1));

                    double res = n1 - n2;
                    expression.set(i, Double.valueOf(res).toString());
                    expression.remove(i - 1);
                    expression.remove(i);

                    i = -1;
                }
            }

            result = Double.parseDouble(expression.get(0));
        }
        catch (Exception e) {
            result = 0.0;
        }

        return result;
    }

    private void updateResultText () {
        TextView resultText = (TextView)findViewById(R.id.textViewResult);
        resultText.setText(currentNumber);

        TextView expressionText = (TextView)findViewById(R.id.textViewExpression);
        expressionText.setText(exprToString());
    }

    private String exprToString () {
        StringBuilder expr = new StringBuilder();
        for (int i = 0; i < expression.size(); i++) {
            expr.append(expression.get(i));
        }
        return expr.toString();
    }
}